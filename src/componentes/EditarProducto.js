import React,{useEffect,useRef} from 'react';


//REDUX
import {useDispatch,useSelector} from 'react-redux';
import {obtenerProductoEditarAction,editarProductoAction} from '../store/actions/productosActions';
import {validarFormularioAction,validacionExito,validacionError} from '../store/actions/validacionActions';

const EditarProducto = ({match, history}) => {

     //crear los refs
     const nombreRef=useRef('');
     const precioRef=useRef('');

     
     const dispatch= useDispatch();
     const editarProducto= (producto) => dispatch(editarProductoAction(producto));

     const validarFormulario= () => dispatch(validarFormularioAction() );
     const exitoValidacion=() => dispatch(validacionExito());
     const errorValidacion= () => dispatch(validacionError());

     //obtener el id a editar
     const {id} = match.params;



     useEffect(() =>{
          dispatch(obtenerProductoEditarAction(id));
     },[dispatch, id])
     //acceder
     const producto= useSelector((state) => state.productos.producto);
     const error= useSelector((state) => state.productos.error);

     //cuando carga la api
     if(!producto) return 'Cargando . . . ';

     const submitEditarProducto = e => {
          e.preventDefault();
          //validar
          validarFormulario();
          if(nombreRef.current.value.trim() === '' || precioRef.current.value.trim() ===''){
               errorValidacion();
               return;
          }
          //no hay error
          exitoValidacion();
          //guardar los cambios

          console.log(nombreRef.current.value);
          editarProducto({
               id,
               nombre: nombreRef.current.value,
               precio: precioRef.current.value
          });



          //redireccionar
          history.push('/');
     }

     return ( 
          <div className="row justify-content-center mt-5">
               <div className="col-md-8">
                    <div className="card">
                         <div className="card-body">
                              <h2 className="text-center">Editar Producto</h2>
                              <form
                                   onSubmit={submitEditarProducto}
                              >
                                   <div className="form-group">
                                        <label>Producto</label>
                                        <input 
                                        type="text" 
                                        className="form-control" 
                                        placeholder="Producto"
                                        defaultValue={producto.nombre}
                                        ref={nombreRef}
                                        />
                                   </div>
                                   <div className="form-group">
                                        <label>Precio del Producto</label>
                                        <input 
                                        type="number" 
                                        className="form-control" 
                                        placeholder="Precio"
                                        defaultValue={producto.precio} 
                                        ref={precioRef}
                                        />
                                   </div>

                                   <button type="submit" className="btn btn-primary font-weight-bold text-uppercase d-block w-100">Guardar Cambios</button>
                              </form>
                              {error ? <div className="font-weight-bold alert alert-danger text-center mt-4">Hubo un error intenta de nuevo</div> : null}
                         </div>
                    </div>
               </div>
          </div>
     );
}
 
export default EditarProducto;