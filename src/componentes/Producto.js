import React from 'react';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';



import {useDispatch} from 'react-redux';
import {borrarProductoAction} from '../store/actions/productosActions';

const Producto = ({producto}) => {

     const dispatch=useDispatch();

     const confirmarEliminarProducto= id => {
          //confirmar con sweet alert
          Swal.fire({
               title: '¿Estás seguro?',
               text: "¡Está acción no se puede deshacer!",
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Si, ¡Eliminar!',
               cancelButtonText: 'Cancelar'
             }).then((result) => {
               if (result.value) {
                 Swal.fire(
                   '¡Eliminado!',
                   'El producto se elimino correctamente.',
                   'success'
                 )
                 dispatch(borrarProductoAction(id))
               }
               })
          
     }

     return ( 
          <tr key={producto.id}>
               <td>{producto.nombre}</td>
               <td><span className="font-weight-bold">$ {producto.precio}</span></td>
               <td className="acciones">
                    <Link 
                         to={`/productos/editar/${producto.id}`}
                         className="btn btn-primary mr-2">
                         Editar
                    </Link>
                    <button 
                         className="btn btn-danger"
                         onClick={() => confirmarEliminarProducto(producto.id) }>
                         Eliminar
                    </button>
               </td>
          </tr>
      );
}
 
export default Producto;