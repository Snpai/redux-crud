import axios from 'axios';

const clienteAxios = axios.create({
     baseURL: 'https://my-json-server.typicode.com/RzzSnpai/productos-redux-crud'
});

export default clienteAxios;