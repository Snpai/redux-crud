import {combineReducers} from 'redux';
import productosReducers from './productosReducer';
import validacionReducer from './validacionReducer';


export default combineReducers({
     productos: productosReducers,
     error: validacionReducer
});