import {
     AGREGAR_PRODUCTO,
     AGREGAR_PRODUCTO_EXITO,
     AGREGAR_PRODUCTO_ERROR,
     COMENZAR_DESCARGA_PRODUCTOS,
     DESCARGA_PRODUCTOS_EXITOSA,
     DESCARGA_PRODUCTOS_ERROR,
     OBTENER_PRODUCTO_ELIMINAR,
     PRODUCTO_ELIMINADO_EXITO,
     PRODUCTO_ELIMINADO_ERROR,
     OBTENER_PRODUCTO_EDITAR,
     PRODUCTO_EDITAR_EXITO,
     PRODUCTO_EDITAR_ERROR,
     COMENZAR_EDICION_PRODUCTO,
     PRODUCTO_EDITADO_EXITO,
     PRODUCTO_EDITADO_ERROR   
     
} from '../types';

import clienteAxios from '../../config/axios';

import Swal from 'sweetalert2';

//crear funcion principal(nuevo producto)
export function crearNuevoProductoAction(producto){
     return (dispatch) => {
          dispatch( nuevoProducto() );

          //insertar en la API
          clienteAxios.post('/productos', producto)
               .then(respuesta=> {
                    console.log(respuesta)
                    dispatch(agregarProductoExito(producto) );
               })
               .catch(error => {
                    console.log('se rompio el post '+ error );
                    dispatch(errorNuevoProducto(error));
               })


          
          
     }
}



export const agregarProductoExito= producto => ({
     type: AGREGAR_PRODUCTO_EXITO,
     payload: producto
})

export const errorNuevoProducto= error => ({
     type: AGREGAR_PRODUCTO_ERROR,
     payload: error
})

export const nuevoProducto= () => ({
     type: AGREGAR_PRODUCTO
})

//funcion principal para agregar productos
export function obtenerProductosAction(){
     return (dispatch) => {
          dispatch( obtenerProductosComienzo() );

          //consultar la api
          clienteAxios.get('/productos')
               .then(respuesta => {
                    
                    dispatch(descargaProductosExito(respuesta.data));
               })
               .catch(error => {
                    dispatch(descargaProductosError());
               })
     }
}

export const obtenerProductosComienzo= () => ({
     type:COMENZAR_DESCARGA_PRODUCTOS
})

export const descargaProductosExito= productos => ({
     type: DESCARGA_PRODUCTOS_EXITOSA,
     payload:productos
})

export const descargaProductosError= () => ({
     type: DESCARGA_PRODUCTOS_ERROR
})
//------------------------------------------------------------------------
//funcion para eliminar un producto en especifico
export function borrarProductoAction(id){
     return (dispatch) => {
          dispatch(obtenerProductoEliminar())

          //eliminar en la api
          clienteAxios.delete(`/productos/${id}`)
               .then(respuesta => {
                    dispatch(eliminarProductoExito(id));
               })
               .catch(error=>{
                    dispatch(eliminarProductoError());
               })
     }
}

export const obtenerProductoEliminar= () =>({
     type:OBTENER_PRODUCTO_ELIMINAR
})

export const eliminarProductoExito= id =>({
     type:PRODUCTO_ELIMINADO_EXITO,
     payload:id
})

export const eliminarProductoError= () => ({
     type:PRODUCTO_ELIMINADO_ERROR
})
//------------------------------------------------------------------------

//funcion principal para editar un producto
export function obtenerProductoEditarAction(id){
     return(dispatch) =>{
          dispatch(obtenerProductoAction());

          //obtener producto de la api
          clienteAxios.get(`/productos/${id}`)
               .then(respuesta =>{
                    console.log(respuesta.data);
                    dispatch(obtenerProductoEditarExito(respuesta.data));
               })
               .catch(error => {
                    dispatch(obtenerProductoEditarError())
                    console.log(error)
               })
     }
}
export const obtenerProductoAction= () => ({
     type: OBTENER_PRODUCTO_EDITAR
})
export const obtenerProductoEditarExito= producto => ({
     type: PRODUCTO_EDITAR_EXITO,
     payload: producto
})

export const obtenerProductoEditarError= () => ({
     type: PRODUCTO_EDITAR_ERROR
})
//------------------------------------------------------------------------

//modifica un producto en la api y state
export function editarProductoAction(producto) {
     return (dispatch) =>{
          dispatch(comenzarEdicionProducto());

          //consultar a la api
          clienteAxios.put( `/productos/${producto.id}`, producto)
               .then(respuesta =>{
                    dispatch(editarProductoExito(respuesta.data));
                    //console.log(respuesta)
                    Swal.fire(
                         'Almacenado',
                         'El producto se actualizó correctamente',
                         'success'     
                    )
               })
               .catch(error=>{
                    //console.log(error)
                    dispatch(editarProductoError());
               })
     }
}

export const comenzarEdicionProducto= () => ({
     type: COMENZAR_EDICION_PRODUCTO
})

export const editarProductoExito= producto => ({
     type: PRODUCTO_EDITADO_EXITO,
     payload: producto
})
export const editarProductoError= () => ({
     type: PRODUCTO_EDITADO_ERROR
})
//------------------------------------------------------------------------
